document.addEventListener("DOMContentLoaded", function() {
    const aboutSquare = document.getElementById('about');
    const finalSquare = document.getElementById('final');
    const week01Square = document.getElementById('week01');
    const myNameSquare = document.getElementById('myName');
    const contentArea = document.getElementById('contentArea');

    aboutSquare.addEventListener('click', function() {
        displayContent('Hi');
    });

    finalSquare.addEventListener('click', function() {
        displayContent('Final');
    });

    week01Square.addEventListener('click', function() {
        displayContent('01');
    });

    // myNameSquare is not clickable

    function displayContent(content) {
        contentArea.textContent = content;
        contentArea.style.display = 'block';
    }
});
